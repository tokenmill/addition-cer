(ns cer.freight-forwarder-db
  (:require [clojure.java.io :as io]
            [clojure-csv.core :as csv]
            [semantic-csv.core :as sc]
            [clojure.string :as s]))


(defn- load-csv [file]
  (with-open [in-file (io/reader file)]
    (->>
      (csv/parse-csv in-file)
      (sc/remove-comments)
      (sc/mappify)
      doall)))

(def consignees (load-csv "rules/lookup/consignee.csv"))
(def forwarders (load-csv "rules/lookup/forwarders.csv"))
(def operators  (load-csv "rules/lookup/operators.csv"))

(def default-forwarder (first forwarders))

(defn- find-by-key [match-key ff-code]
  (first (filter #(= (get % match-key) ff-code) forwarders)))

(defn find-forwarder [match-key ff-code]
  (or (find-by-key match-key ff-code) default-forwarder))

(defn any-forwarder-by-names
  "Forwarder email names end with '- DSV' strip that to match with names in DB,
  and check if there are any matches"
  [email-names]
  (some #(find-by-key :name (s/replace-first % #"\s+-\s+DSV$" ""))
        email-names))

(defn clean-company-name [name]
  (-> name
      s/lower-case
      (s/replace-first #"(?i)\s+(ltd|a/s|inc|aps)" "")
      (s/replace-first #"\s+[vV]\/.*" "")))

(defn find-consignee
  "check for consignee name, ignores case"
  [c-name]
  (if c-name
    (first
     (filter
      (fn [{name :name}] (= (clean-company-name c-name)
                           (s/lower-case name)))
      consignees))
    default-forwarder))

(defn consignee->forwarder [c-name]
  (find-forwarder :code (:forwarder (find-consignee c-name))))

(defn find-operator [code]
  (or (first
       (filter
        (fn [{id :id}] (= (s/lower-case id) (s/lower-case code)))
        operators))
      default-forwarder))
