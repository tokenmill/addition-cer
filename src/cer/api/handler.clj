(ns cer.api.handler
  (:require [ring.middleware.multipart-params :as mpmw]
            [ring.middleware.multipart-params.byte-array :as mpmwba]
            [ring.swagger.upload :as upload]
            [ring.middleware.cors :refer [wrap-cors]]
            [compojure.api.sweet :refer :all]
            [cheshire.core :as ch]
            [ring.util.http-response :refer :all]
            [cer.rules :as rules]
            [cer.action :as action]
            [cer.email :as email]))


(defn allow-cors [handler]
  (wrap-cors handler :access-control-allow-origin [#".*"]
    :access-control-allow-methods [:get :put :post :delete]))

(defn- analyze-email [msg]
  (rules/aggregate-action (rules/apply-rules msg)))

(def app

  (allow-cors
   (api
    {:swagger
     {:ui   "/api-docs"
      :spec "/swagger.json"
      :data {:info {:title "Cognitive Email Reader API"
                    :description
                    (str
                     "CER provides services to read email content and "
                     "get back parsing results which allow to make the decision as "
                     "for further processing of a given email")}}}}

    (GET "/" [] (ok "Up and running"))

    (context "/v1" []
      :tags ["CER API v1.0"]
      (POST "/convert" []
        :multipart-params [email :- upload/ByteArrayUpload]
        :middleware [[mpmw/wrap-multipart-params {:store (mpmwba/byte-array-store)}]]
        :return email/Message
        :summary "Converts posted Outlook MSG file to JSON structure representing email fields"
        (ok (email/from-bytes (:bytes email))))
      (POST "/analyze" []
        :body [msg email/Message]
        :summary "Accepts JSON representing email structure (you can get that via call to '/convert') and returns analysis results"
        (ok (analyze-email msg)))
      (POST "/convert-analyze" []
        :multipart-params [email :- upload/ByteArrayUpload]
        :middleware [[mpmw/wrap-multipart-params {:store (mpmwba/byte-array-store)}]]
        :summary "Combines /convert and /analyse"
        (ok
         (let [converted (email/from-bytes (:bytes email))]
           (assoc (analyze-email converted)
            :email converted))))))))
