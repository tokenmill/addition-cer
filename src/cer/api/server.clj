(ns cer.api.server
  (:gen-class)
  (:require [cer.api.handler :refer [app]]
            [clojure.tools.logging :as log]
            [environ.core :refer [env]]
            [ring.adapter.jetty :as jetty]))

(defn -main [& _]
  (try
    (let [host (get env :host "0.0.0.0")
          port (Integer/valueOf ^String (get env :port "3001"))]
      (log/infof "Starting server on host '%s' with port '%s'" host port)
      (jetty/run-jetty #'app {:port port :host host :join? false}))
    (catch Exception e
      (log/error e))))
