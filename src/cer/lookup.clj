(ns cer.lookup
  (:require [clojure.java.io :as io]
            [clojure.string :as s]))


(defn- file-line-set
  "Read lines in the file and put them into set"
  [dic] (-> dic io/reader line-seq set))

(defn- file-lister [dir] (->> dir io/file file-seq (filter #(.isFile %))))

(defn- dic-files
  "Dictionary files in a dir"
  [] (file-lister "rules/dictionaries"))

(defn- regex-files
  "Regex files in a dir"
  [] (file-lister "rules/regexes"))

(defn- file->kw
  "Convert file name to keyword (no extension)"
  [f] (-> f .getName (s/replace #".txt$" "") keyword))

(def dictionaries
  (reduce
    (fn [lexicon file]
      (assoc lexicon (file->kw file) (file-line-set file)))
    {}
    (dic-files)))

(def regexes
  (reduce
    (fn [lexicon file]
      (assoc lexicon (file->kw file) (file-line-set file)))
    {}
    (regex-files)))

(defn matches
  "Match single string. Come back with a map which shows which dictionaries were hit"
  [txt]
  (reduce
    (fn [acc [k v]] (if (contains? v txt) (assoc acc k #{txt}) acc))
    {} dictionaries))

(defn matches-sub
  "Match substring string. Come back with a map which shows which dictionaries were hit"
  [txt]
  (reduce
   (fn [acc [k v]] (if (some #(clojure.string/includes? txt %) v)
                       (assoc acc k #{txt}) acc))
   {} dictionaries))

(defn matches-regex
  [txt]
  (reduce
    (fn [acc [tag regexes]]
      ;;get regex hits in text, thus resulting map contains substrings matching regexes
      (let [hits (remove nil? (map #(-> % re-pattern (re-find txt) second) regexes))]
        (if (empty? hits)
          acc
          (assoc acc tag (set hits)))))
   {} regexes))

(defn matches-any
  "Same as 'matches' but for a collection of strings"
  [coll]
  (reduce
   (fn [match-aggr txt]
     (reduce (fn [mm [k v]] (assoc mm k (set (concat (get mm k #{}) v))))
             match-aggr (matches txt)))
   {} coll))
