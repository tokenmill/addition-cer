(ns cer.email
  (:require [clojure.java.io :as io]
            [schema.core :as s]
            [clojure.tools.logging :as log])
  (:import org.apache.poi.hsmf.MAPIMessage
           java.io.ByteArrayInputStream))

(s/defschema Message 
  {:recipient-emails (s/maybe #{s/Str})
   :recipient-names #{s/Str}
   :sender-name s/Str
   :subject s/Str
   :body-text s/Str
   :date s/Int
   :attachment-file-names (s/maybe [s/Str])})

(defn- get-valid-attachments
  "Get attachment names dropping stuff which is likely signature images"
  [msg]
  (vec
    (remove #(re-find #".*\.(jpg|pic|gif|png)" %)
      (map #(-> % .getAttachFileName .toString) (.getAttachmentFiles msg)))))

(defn parse-message
  [msg-stream]
  (let [msg (MAPIMessage. msg-stream)]
    {:recipient-emails (set (.getRecipientEmailAddressList msg))
     :recipient-names (set (.getRecipientNamesList msg))
     :sender-name (.getDisplayFrom msg)
     :subject (.getSubject msg)
     :body-text (.getTextBody msg)
     :attachment-file-names (get-valid-attachments msg)
     :date (.getTimeInMillis (.getMessageDate msg))}))

(defn from-bytes [bites]
  (with-open [content (ByteArrayInputStream. bites)] (parse-message content)))

(defn from-file [file-name]
  (with-open [content (io/input-stream file-name)] (parse-message content)))
