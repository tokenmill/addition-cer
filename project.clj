(defproject outlook_msg "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.apache.poi/poi "3.17"]
                 [org.apache.poi/poi-scratchpad "3.17"]
                 [cheshire "5.8.0"]
                 [environ "1.1.0"]
                 [prismatic/schema "1.1.9"]
                 [metosin/compojure-api "2.0.0-alpha20"]
                 [metosin/spec-tools "0.7.0"]
                 [org.clojure/tools.logging "0.4.0"]
                 [ch.qos.logback/logback-classic "1.2.3"]
                 [ring/ring-jetty-adapter "1.6.2"]
                 [ring-cors "0.1.12"]
                 [semantic-csv "0.2.1-alpha1"]
                 [clojure-csv/clojure-csv "2.0.1"]
                 [org.clojure/clojure "1.9.0"]]
  :main cer.api.server
  :uberjar-name "cer-server.jar"
  :profiles {:dev {:resource-paths ["test/resources" "resources"]
                   :plugins [[lein-ring "0.10.0"]]}})
