(ns cer.lookup-test
  (:require [cer.lookup :refer :all]
            [clojure.test :refer :all]))

(deftest lookup-matching
  (testing "matches on single string"
    (is (= {} (matches "NO HIT")))
    (is (= {:seafreight-mailbox #{"DK.AS DSV Import Seafreight, Copenhagen"}}
           (matches "DK.AS DSV Import Seafreight, Copenhagen"))))

  (testing "substring matching"
    (is (= {:documents #{"DSV Air Branch - HK2 Guangzhou - Agent Departure Notice - CHG107137"}}
          (matches-sub "DSV Air Branch - HK2 Guangzhou - Agent Departure Notice - CHG107137"))))

  (testing "regex matching"
    (is (= {} (matches-regex "A")))
    (is (= {} (matches-regex "AA456")))
    (is (= {} (matches-regex "abcd123")))
    (is (= {:customs-sub #{"SATL0078974"}}
          (matches-regex "IMPDK : 2018-05-03//(SATL0078974) / 2018123336658")))
    (is (= {:consignee-body #{"ABCD"}} (matches-regex "C/ ABCD")))
    (is (= {}(matches-regex "C/O ABCD")))
    (is (= {:consignee-body #{"ABCD"}} (matches-regex "aaa C/ ABCD Something")))
    (is (= {:consignee-body #{"ABCD A/S"}} (matches-regex "Axxx C/ABCD A/S aaa")))
    (is (= {:dispatch-sub #{"ABCD1234567"}} (matches-regex "Abc ABCD1234567"))))

  (testing "matches on string collection"
    (is (= {} (matches-any ["NO HIT"])))
    (is (= {:seafreight-mailbox #{"DK.AS DSV Import Seafreight, Copenhagen"}
            :gwa #{"Yui Liu - DSV"}}
           (matches-any ["DK.AS DSV Import Seafreight, Copenhagen" "Yui Liu - DSV" "Tanja Ørskov Bøgegaard - DSV"])))))
