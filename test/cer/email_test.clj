(ns cer.email-test
  (:require [clojure.test :refer :all]
            [cer.email :refer :all]))

(deftest email-parsing
  (testing "outlook email parsing"
    (let [{:keys [recipient-emails recipient-names sender-name subject body-text date
                  attachment-file-names]}
          (from-file "data/dispatch/FW  RE  STAD8946652.msg")]
      (is (= #{"ImpSea.CPH@dk.dsv.com"} recipient-emails))
      (is (= #{"DK.AS DSV Import Seafreight, Copenhagen"} recipient-names))
      (is (= "Jack Lou - DSV" sender-name))
      (is (= "FW: RE: STAD8946652" subject))
      (is (= ["SWB - STAD8946644 -  NGB8946644 -"          
              "SWB - STAD8946616 -  NGB8946616 -"
              "SWB - STAD8946652 -  NGB8946652 -"
              "SWB - STAD8946613 -  NGB8946613 -"
              "SWB - STAD8946598 -  NGB8946598 -"]
            attachment-file-names))
      (is (clojure.string/starts-with? body-text "Dear,"))
      (is (= 1526982482514 date)))))
