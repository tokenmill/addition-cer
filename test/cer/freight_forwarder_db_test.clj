(ns cer.freight-forwarder-db-test
  (:require [cer.freight-forwarder-db :refer :all]
            [clojure.test :refer :all]))

(deftest test-db-searches
  (testing "operator search"
    (is (= {:id "SSZX0263946" :email "dummy1@a.com"}
          (find-operator "SSZX0263946"))))
  (testing "forwarder search"
    (is (= {:name "Anders Oldenborg Kristensen"
            :code "AOK"
            :email "anders.o.kristensen@dk.dsv.com"}
           (any-forwarder-by-names ["Anders Oldenborg Kristensen - DSV"])))
    (is (= {:name "Anders Oldenborg Kristensen"
            :code "AOK"
            :email "anders.o.kristensen@dk.dsv.com"}
           (find-forwarder :name "Anders Oldenborg Kristensen")))
    (is (= {:name "Anders Oldenborg Kristensen"
            :code "AOK"
            :email "anders.o.kristensen@dk.dsv.com"}
           (find-forwarder :code "AOK"))))
  (testing "default forwarder"
    (is (= {:name "Martin Hoegsted Hansen"
            :code "MHN"
            :email "Martin.H.Hansen@dk.dsv.com"}
           default-forwarder)))

  (testing "consignee search"
    (is (= default-forwarder (find-consignee nil)))
    (is (= {:name "GLECO"
            :forwarder "DGP"
            :code "6402106255"}
          (find-consignee "gleco"))))

  (testing "all in single call"
    (let [good-data {:name "Dennis Günckel Petersen"
                     :code "DGP"
                     :email "Dennis.Petersen@dk.dsv.com"}]
      (is (not (nil? (consignee->forwarder "comma COMPANY"))))
      (is (= good-data (consignee->forwarder "Gleco")))
      (is (= good-data (consignee->forwarder "Gleco A/S")))
      (is (= good-data (consignee->forwarder "Gleco LTD")))
      (is (= good-data (consignee->forwarder "GLECO LTD")))
      (is (= good-data (consignee->forwarder "GLECO V/something"))))))

