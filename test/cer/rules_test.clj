(ns cer.rules-test
  (:require [cer.rules :refer :all]
            [clojure.test :refer :all]))

(deftest test-rule-application
  (testing "dispatch"
    (is (= [{:id     :disp-1
             :match  :dispatch
             :action {:instruction :archive}
             :decision
             {:sender {:gwa #{"Yui Liu - DSV"}}
              :recipient
              {:freight-forwarder #{"Anders Oldenborg Kristensen - DSV"}}}}
            {:match  :copy
             :id     :copy-3
             :action {:instruction :archive}
             :decision
             {:recipient
              {:freight-forwarder #{"Anders Oldenborg Kristensen - DSV"}}}}]
           (apply-rules
            {:recipient-names ["Anders Oldenborg Kristensen - DSV"]
             :sender-name     "Yui Liu - DSV"
             :subject         "DSV & ..."
             :body-text       ""})))
    (is (= {:id    :disp-12
            :match :dispatch
            :action {:instruction :forward
                     :to {:name "Martin Hoegsted Hansen"
                          :code "MHN"
                          :email "Martin.H.Hansen@dk.dsv.com"}}
            :decision
            {:recipient
             {:seafreight-mailbox #{"DK.AS DSV Import Seafreight, Copenhagen"}}
             :subject
             {:dispatch-sub #{"STAD8964727"}}}}
           (first
            (apply-rules
             {:recipient-names ["DK.AS DSV Import Seafreight, Copenhagen"]
              :sender-name     "Something"
              :body-text       ""
              :subject         "FW: on Po#008200,8500 - STAD8964727/CCN918464"}))))

    (is (= {:id    :disp-12
            :match :dispatch
            :action {:instruction :forward
                     :to {:name "Mette Budolph-Larsen"
                          :code "MBL"
                          :email "mette.budolph-larsen@dk.dsv.com"}}
            :decision
            {:recipient
             {:seafreight-mailbox #{"DK.AS DSV Import Seafreight, Copenhagen"}}
             :subject
             {:consignee-body #{"Comma company"} }}}
           (first
            (apply-rules
             {:recipient-names ["DK.AS DSV Import Seafreight, Copenhagen"]
              :sender-name     "Something"
              :body-text       ""
              :subject         "HIGH-STRENGTH C/Comma company S/NINGBO NINGLI"}))))

    (is (= {:id    :disp-12
            :match :dispatch
            :action {:instruction :forward
                     :to {:name "Mette Budolph-Larsen"
                          :code "MBL"
                          :email "mette.budolph-larsen@dk.dsv.com"}}
            :decision
            {:recipient
             {:seafreight-mailbox #{"DK.AS DSV Import Seafreight, Copenhagen"}}
             :subject
             {:consignee-body #{"COMMA COMPANY"} :dispatch-sub #{"STAD8975813"}}}}
           (first
            (apply-rules
             {:recipient-names ["DK.AS DSV Import Seafreight, Copenhagen"]
              :sender-name     "Something"
              :body-text       ""
              :subject         "STAD8975813 NEW BOOKING S/NINGBO NINGLI HIGH-STRENGTH C/COMMA COMPANY"}))))

    (is (= {:id       :disp-14-17-19
            :match    :dispatch
            :action {:instruction :forward,
                     :to {:name "Steffen Vraa Nielsen",
                          :code "SN0",
                          :email "steffen.v.nielsen@dk.dsv.com"}}
            :decision {:body
                       {:consignee-body #{"HARTS"}}
                       :recipient
                       {:seafreight-mailbox #{"DK.AS DSV Import Seafreight, Copenhagen"}}}}
           (first
            (apply-rules
             {:recipient-names ["DK.AS DSV Import Seafreight, Copenhagen"]
              :sender-name     "Something"
              :subject         "DSV & ..."
              :body-text
              "Dear
             S/ TAIZHOU GONGMEI GIFT
             C/ HARTS
             14CTNS / 186KGS / 1.4CBM"}))))

    ;;Test the same dispatch with forward action
    (is (= {:id       :disp-14-17-19
            :match   :dispatch
            :action   {:instruction :forward
                       :to
                       {:name  "Mikael Bernhardt Johansen",
                        :code  "MBJ",
                        :email "Mikael.B.Johansen@DK.DSV.COM"}}
            :decision {:recipient {:seafreight-mailbox #{"DK.AS DSV Import Seafreight, Copenhagen"}}
                       :body      {:consignee-body #{"MARMIC"}}}}
           (first
            (apply-rules
             {:recipient-names ["DK.AS DSV Import Seafreight, Copenhagen"]
              :sender-name     "Something"
              :subject         "DSV & ..."
              :body-text
              "Dear
             S/ TAIZHOU GONGMEI GIFT
             C/ MARMIC
             14CTNS / 186KGS / 1.4CBM"})))))

  (testing "copy"
    (is (= [{:id     :copy-3
             :match  :copy
             :action {:instruction :archive}
             :decision
             {:recipient
              {:freight-forwarder #{"Anders Oldenborg Kristensen - DSV"}}}}]
           (apply-rules
            {:recipient-names ["Anders Oldenborg Kristensen - DSV"]
             :sender-name     "Lee Gu"
             :subject         "DSV & ..."
             :body-text       ""})))
    (is (= []
           (apply-rules
            {:recipient-names ["Someone - DSV"]
             :sender-name     "Lee Gu"
             :subject         "DSV & ..."
             :body-text       ""}))))

  (testing "autogenerated"
    (is (= [{:id     :autogen-9
             :match  :autogenerated
             :action {:instruction :archive}
             :decision
             {:subject
              {:autogen-sub
               #{"DSV Air & Sea, Inc. - Phoenix - INVOICE - US03384448"}}}}]
           (apply-rules
            {:recipient-names ["Someone"]
             :sender-name     "Sender"
             :subject         "DSV Air & Sea, Inc. - Phoenix - INVOICE - US03384448"
             :body-text       "Please see the attached documents.\n\nTo read PDF files, get Adobe Acrobat Reader from: https://get.adobe.com/reader/"}))))

  (testing "documents"
    (is (= [{:id     :doc-4-8
             :match  :document
             :action {:instruction :archive}
             :decision
             {:subject
              {:documents
               #{"DSV & ... - Guangzhou - Agent Departure Notice - CHG107137"}}}}]
           (apply-rules
            {:recipient-emails ["ImpSea.CPH@dk.dsv.com"]
             :recipient-names  ["DK.AS DSV Import Seafreight, Copenhagen"]
             :sender-name      "Lee Gu"
             :subject          "DSV & ... - Guangzhou - Agent Departure Notice - CHG107137"
             :body-text        ""})))

    (testing "customs"
      (is (= {:id      :cust-10
              :match   :customs
              :action  {:instruction :forward :to {:id "SATL0078974" :email "dummy2@a.com"}}
              :decision {:subject {:customs-sub #{"SATL0078974"}}}}
            (first
              (apply-rules
                {:recipient-names ["Someone"]
                 :sender-name     "Sender"
                 :subject         "IMPDK : 1805030565 / 1009860 - DAY-2018-05-03//(SATL0078974) / 20181233"
                 :body-text       ""}))))
      (is (= {:id      :cust-11
              :match   :customs
              :action  {:instruction :forward
                        :to {:id "SSZX0263946" :email "dummy1@a.com"}}
              :decision {:body {:customs-body #{"SSZX0263946"}}
                         :attachment [:f1 :f2]}}
             (first
              (apply-rules
               {:recipient-names ["Someone"]
                :sender-name     "Sender"
                :attachment-file-names [:f1 :f2]
                :subject         "Anything"
                :body-text
                "The declaration : 2018123150473 is completed.
                 Internal case id : 1803230331
                 Internal description : 1002691 - CHK198108// (SSZX0263946)"}))))
      (is (= {:id      :cust-11
              :match   :customs
              :action  {:instruction :forward
                        :to {:id "SBRY0149730" :email "dummy3@a.com"}}
              :decision {:body {:customs-body #{"SBRY0149730"}}
                         :attachment [:f1 :f2]}}
            (first
              (apply-rules
                {:recipient-names ["Someone"]
                 :sender-name     "Sender"
                 :attachment-file-names [:f1 :f2]
                 :subject         "Anything"
                 :body-text
                 "Attached the import registration with the ref.no : 2018122970005
                  Basic data for this registration :
                  Description : 994595 - CDK303896//  (SBRY0149730)"})))))

    (testing "webquery"
      (is (= {:action {:instruction :forward :to {:id "WQ" :email "Annette.S.Pedersen@dk.dsv.com"}}
              :match :webquery
              :id :wq-23
              :decision {:subject {:webquery-sub #{"Deadline monitoring"}}
                         :sender {:webquery-sender #{"WQ.Finance@dsv.com"}}}}
            (first
              (apply-rules
                {:recipient-names ["Someone"]
                 :sender-name     "WQ.Finance@dsv.com"
                 :subject         "Deadline monitoring"
                 :body-text ""}))
            ))
      (is (= {:action {:instruction :archive}
              :match :webquery
              :id :wq-23
              :decision {:subject {:webquery-sub #{"WQ: Query 244842 has been changed"}}
                         :sender {:webquery-sender #{"WQ.Finance@dsv.com"}}}}
            (first
              (apply-rules
                {:recipient-names ["Someone"]
                 :sender-name     "WQ.Finance@dsv.com"
                 :subject         "WQ: Query 244842 has been changed"
                 :body-text ""})))))

    (testing "price"
      (is (= [{:action {:instruction :forward
                        :to {:id "BusinessDev" :name "Jasper Bjelke"
                             :email "jesper.bjelke@dk.dsv.com"}}
               :match :price
               :id :price
               :decision {:subject {:price-sub #{"Import fra Kina"}}}}]
            (apply-rules
              {:recipient-names ["Someone"]
               :sender-name     "Someone"
               :subject         "Import fra Kina"
               :body-text ""})))
      (is (= [{:action {:instruction :forward
                        :to {:id "BusinessDev" :name "Jasper Bjelke"
                             :email "jesper.bjelke@dk.dsv.com"}}
               :match :price
               :id :price
               :decision {:subject {:price-sub #{"FW: Ny kontakt fra DSV kontaktformularen"}}}}]
            (apply-rules
              {:recipient-names ["Someone"]
               :sender-name     "Someone"
               :subject         "FW: Ny kontakt fra DSV kontaktformularen"
               :body-text ""})))
      )

    ))

(deftest aggregation-testing
  (testing "evidence aggregation"
    (is (= {:instruction :archive}
           (:action
            (aggregate-action [{:action {:instruction :archive}}
                               {:action {:instruction :archive}}]))))
    (is (= {:instruction :forward :to []}
           (:action
            (aggregate-action [{:action {:instruction :forward :to []}}
                               {:action {:instruction :archive}}]))))))
